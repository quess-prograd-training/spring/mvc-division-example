package com.example.MvcExampleDivision.controller;

import com.example.MvcExampleDivision.model.Division;
import com.example.MvcExampleDivision.service.DivisionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DivisionController {
    @Autowired
    DivisionService divisionServiceObject;


    @GetMapping("/message")
    public String division(){
        try{
            Division divisionObject = new Division(20,0);
            int result = divisionServiceObject.division(divisionObject);
            return "The Division of two numbers is "+ result;
        }
        catch (Exception e){
            return e.getMessage();
        }


    }
}
