package com.example.MvcExampleDivision;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MvcExampleDivisionApplication {

	public static void main(String[] args) {
		SpringApplication.run(MvcExampleDivisionApplication.class, args);
	}

}
