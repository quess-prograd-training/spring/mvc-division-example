package com.example.MvcExampleDivision;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class DivisionAspect {

        @Before("execution(* com.example.MvcExampleDivision.controller.DivisionController.division())")
        public void beforeDivision(){
            System.out.println("Division process Started!!");
        }
        @After("execution(* com.example.MvcExampleDivision.controller.DivisionController.division())")
        public void afterDivision(){
            System.out.println("Division process completed!!");
        }

    }

