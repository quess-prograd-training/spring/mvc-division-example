package com.example.MvcExampleDivision.service;

import com.example.MvcExampleDivision.model.Division;
import org.springframework.stereotype.Service;

@Service
public class DivisionService {
    public int division(Division division){
        int result = division.getFirstNumber()/division.getSecondNumber();
        division.setResult(result);
        return division.getResult();
    }
}
